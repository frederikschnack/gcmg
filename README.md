# Goal-Oriented Cascadic Multigrid Method

This repository contains my Bachelor's thesis and its corresponding code written in Python with the framework FEniCS.

## Abstract
In this thesis we extend the cascadic multigrid method (cmg) from [BD96] with a goal-oriented error
estimator as introduced in [BR03]. This includes the presentation of the mathematical frame-
work as well as the convergence and complexity theory of the underlying algorithm. After the
extension to an adaptive method and the introduction of the dual weighted residual method, we
detach ourselves from the previous literature and develop a new adaptive strategy for the error
estimation with respect to arbitrary functionals. The importance of the type of error indicators
and the associated grid refinements is particularly emphasized. For the verification of our con-
sideration the implementation of the developed algorithm (gcmg) is presented and analyzed with the
help of numerical experiments. 

## Content

 notebook | contains
 --- | ---
gcmg | The algorithm developed in this thesis. 
res_cmg |  The cmg method with the classical residual based error estimate. 
cmg | The classical cmg method with the discretization error estimate. 
